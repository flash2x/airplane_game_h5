AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.Fighter=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","Fighter");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.Fighter,annie.MovieClip);
feiji.Fighter.prototype.planeW = 180;
feiji.Fighter.prototype.planeH = 164;
feiji.Fighter.prototype.status = 0;//0正常状态，1子弹爆炸
feiji.Fighter.prototype.attackSystem = null;//弹道系统
feiji.Fighter.prototype.getBulletTimeId = null;
feiji.Fighter.prototype.attackLevel = 1;//1单弹道，2双弹道，3多弹道
feiji.Fighter.prototype.attackSpeed = 300;//弹道发射频率
feiji.Fighter.prototype.enemys = [];//敌机数组
/**
 *  攻击方法
 * @param gameimgPage
 */
feiji.Fighter.prototype.attack = function (gameingPage) {
	var s = this;
	if (s.status == 0) {
		if (s.attackSystem) {
			s.attackSystem.attackTargets = s.enemys;//攻击系统的攻击目标
			if (!s.getBulletTimeId) {
				s.getBulletTimeId = setInterval(function () {
					// trace(s.attackSystem.getBullet());
					switch (s.attackLevel) {
						case 1:
							var bullet = s.attackSystem.getBullet();
							bullet.x = s.x;
							bullet.y = s.y - 86;
							gameingPage.addChild(bullet);
							break;
						case 2:
							var bulletL = s.attackSystem.getBullet(),
								bulletR = s.attackSystem.getBullet();
							bulletL.x = s.x - 55;
							bulletL.y = s.y - 26;
							bulletR.x = s.x + 55;
							bulletR.y = s.y - 26;
							gameingPage.addChild(bulletL);
							gameingPage.addChild(bulletR);
							break;
						case 3:
							var bulletL1 = s.attackSystem.getBullet(),
								bulletL2 = s.attackSystem.getBullet(),
								bulletR1 = s.attackSystem.getBullet(),
								bulletR2 = s.attackSystem.getBullet(),
								bulletC = s.attackSystem.getBullet();
							bulletL1.x = s.x - 55;
							bulletL1.y = s.y - 110;
							bulletR1.x = s.x + 55;
							bulletR1.y = s.y - 110;
							bulletC.x = s.x;
							bulletC.y = s.y - 86;
							bulletC.gotoAndStop(2);
							bulletL2.x = s.x - 55;
							bulletL2.y = s.y - 26;
							bulletR2.x = s.x + 55;
							bulletR2.y = s.y - 26;
							gameingPage.addChild(bulletL1);
							gameingPage.addChild(bulletL2);
							gameingPage.addChild(bulletC);
							gameingPage.addChild(bulletR1);
							gameingPage.addChild(bulletR2);
							break;
						default:
							throw new Error('超出已有弹道级别！');
							break;
					}

				}, s.attackSpeed);
			}
		} else {
			throw new Error('attackSystem未定义');
		}
	} else {
		trace('飞机status状态1爆炸了:' + s.status);
	}
}

/**
 * 飞机爆炸
 */
feiji.Fighter.prototype.bomb = function () {
	var s = this;
	s.gotoAndStop(2);
	s.status = 1;//爆炸状态
	clearInterval(s.getBulletTimeId);//停止子弹发射
	s.getBulletTimeId = null;
	clearInterval(s.parent.creatTonicTimeId);//移除弹药补给
	s.parent.creatTonicTimeId = null;//移除弹药补给
	annie.globalDispatcher.dispatchEvent('liveCountEvent');//扣除生命数
	setTimeout(function () {
		s.attackSystem = null;
		if (s.stage) {
			s.parent.removeChild(s);//把飞机移除舞台
		}
	}, 400);
}
/**
 * 添加到舞台
 * @param e
 */
feiji.Fighter.prototype.onAddToStage = function (e) {
	var s = this,
		planeAttackSystem = new feiji.BulletManager();//初始化弹道系统
	s.attackSystem = planeAttackSystem;//为战斗机装配弹道攻击系统
	s.x = 445;
	s.y = 924;
	// s.addEventListener(annie.Event.ENTER_FRAME, s.fmh = s.planeCheckHitTestPoint.bind(s));
	/**
	 * 移除爆炸的敌机,实时更新敌人数据
	 */
	annie.globalDispatcher.addEventListener('delDiePlane', function (e) {
		var enemysLeng = s.enemys.length;
		for (var i = 0; i < enemysLeng; i++) {
			if (s.enemys[i] == e.data) {
				trace(i);
				s.enemys.splice(i, 1);
			}
		}
	})

}

/**
 * 飞机移动
 * @param e
 */
feiji.Fighter.prototype.planeCheckHitTestPoint = function (e) {
	// s.parent.localToGlobal(new annie.Point(s.enemys[i].x, s.enemys[i].y + 160)
	var s = this,
		enemyLeng = s.enemys.length;
	if (enemyLeng > 0) {
		for (var i = 0; i < enemyLeng; i++) {
			if (s.hitTestPoint(new annie.Point(s.enemys[i].x, s.enemys[i].y + 160))) {
				s.removeEventListener(annie.Event.ENTER_FRAME, s.fmh);
				s.fmh = null;
				s.bomb();//飞机爆炸
				break;
			}
		}
	}
}
