AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.Tonic=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","Tonic");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.Tonic,annie.MovieClip);
feiji.Tonic.prototype.status = 0;//0正常状态，1爆炸
feiji.Tonic.prototype.TsHeight = 206;
feiji.Tonic.prototype.TsWidth = 184;
feiji.Tonic.prototype.speed = 6;
feiji.Tonic.prototype.hitTestObj = null;
/*消失*/
feiji.Tonic.prototype.boom = function () {
	var s = this;
	s.removeEventListener(annie.Event.ENTER_FRAME, s.tmh);
	s.tmh = null;
	s.parent.removeChild(s);
	annie.globalDispatcher.dispatchEvent(new annie.Event('imporveAttack'));//提升攻击力
}
/**
 * 添加到舞台
 * @param e
 */
feiji.Tonic.prototype.onAddToStage = function (e) {
	var s = this;
	s.gotoAndStop(2);
	s.addEventListener(annie.Event.ENTER_FRAME, s.tmh = s.TonicMovingHandler.bind(s));
	//初始化敌机的有效出现位置
	var validX,
		randomX = Math.floor(Math.random() * s.stage.desWidth);
	if (randomX < s.stage.viewRect.x + s.TsWidth / 2) {
		validX = s.stage.viewRect.x + s.TsWidth / 2 + 20;
	} else if (randomX > (s.stage.viewRect.x + s.stage.viewRect.width - s.TsWidth / 2)) {
		validX = s.stage.viewRect.x + s.stage.viewRect.width - s.TsWidth / 2 - 20;
	} else {
		validX = randomX;
	}
	s.x = validX;
	s.y = 0;


};
/**
 *
 * @param e
 * @constructor
 */
feiji.Tonic.prototype.TonicMovingHandler = function (e) {
	var s = this;
	if (s.stage) {
		if (s.y > s.stage.desHeight + s.TsHeight) {
			s.removeEventListener(annie.Event.ENTER_FRAME, s.tmh);
			s.tmh = null;
			s.parent.removeChild(s);//把视窗外的敌机移除舞台
			return;
		}
	}

	//吃到了物品
	if (s.hitTestObj && s.hitTestObj.status == 0 && s.status == 0 && getDistance(s.hitTestObj.x, s.hitTestObj.y, s.x, s.y) < 60) {
		s.boom();
	}
	s.y += s.speed;
}
