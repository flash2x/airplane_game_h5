AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.EnemyBoss=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","EnemyBoss");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.EnemyBoss,annie.MovieClip);
feiji.EnemyBoss.prototype.status = 0;//0正常状态，1子弹爆炸
feiji.EnemyBoss.prototype.EsHeight = 260;
feiji.EnemyBoss.prototype.EsWidth = 480;
feiji.EnemyBoss.prototype.speed = 2;
feiji.EnemyBoss.prototype.enemyType = 'rockBoss';
feiji.EnemyBoss.prototype.blood = 800;//敌机血量
feiji.EnemyBoss.prototype.hitTestObj = null;
feiji.EnemyBoss.prototype.delDiePlaneEvent = null;
feiji.EnemyBoss.prototype.bomb = function () {
	var s = this;
	s.removeEventListener(annie.Event.ENTER_FRAME, s.emh);
	s.emh = null;
	s.gotoAndStop(2);
	s.status = 1;//爆炸状态
	s.blood = 0;
	setTimeout(function () {
		if (!s.delDiePlaneEvent) {
			s.delDiePlaneEvent = new annie.Event('delDiePlane');
		}
		annie.globalDispatcher.dispatchEvent(s.delDiePlaneEvent, s);//抛出移除红色boss飞机，战机enemys敌人数组去除已消灭目标

		s.parent.removeChild(s);//把视窗外的敌机移除舞台
	}, 400);
}
/**
 * 添加到舞台
 * @param e
 */
feiji.EnemyBoss.prototype.onAddToStage = function (e) {
	var s = this;
	s.addEventListener(annie.Event.ENTER_FRAME, s.emh = s.EnemyMovingHandler.bind(s));
	//初始化敌机的有效出现位置
	s.x = (s.stage.desWidth - s.x) / 2;
	s.y = 0;
}
/**
 *
 * @param e
 * @constructor
 */
feiji.EnemyBoss.prototype.EnemyMovingHandler = function (e) {
	var s = this;
	if (s.stage) {
		if (s.y > s.stage.desHeight + s.EsHeight) {
			s.removeEventListener(annie.Event.ENTER_FRAME, s.emh);
			s.emh = null;
			s.parent.removeChild(s);//把视窗外的敌机移除舞台
			return;
		}
	}
	//敌机碰到战机
	if (s.hitTestObj && s.hitTestObj.status == 0 && s.status == 0 && getDistance(s.hitTestObj.x, s.hitTestObj.y, s.x, s.y) < 40) {
		s.removeEventListener(annie.Event.ENTER_FRAME, s.emh);
		s.emh = null;
		if (s.hitTestObj) {
			s.hitTestObj.bomb();//战机爆炸
		}
		// s.bomb();//敌机爆炸
	}
	if (s.stage) {
		s.y += s.speed;
	}
}
