AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.GamingPage=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","GamingPage");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.GamingPage,annie.Sprite);
feiji.GamingPage.prototype.creatEnemyPlanesTimeId = null;
feiji.GamingPage.prototype.creatTonicTimeId = null;
feiji.GamingPage.prototype.countScoreManager = null;
feiji.GamingPage.prototype.boosRed = null;
feiji.GamingPage.prototype.finalBoss = null;
/**
 * 激活游戏
 * @param e
 */
feiji.GamingPage.prototype.gameAct = function (e) {
	var s = this,
		battlePlane = new feiji.Fighter();
	s.addChild(battlePlane);
	battlePlane.attack(s);//开始攻击
	s.battlePlane = battlePlane;
	/*模拟吃了道具的情况*/
	// setTimeout(function () {
	//     battlePlane.attackLevel = 2;
	//     battlePlane.attack(s);//升级攻击
	//     setTimeout(function () {
	//         battlePlane.attackLevel = 3;
	//         battlePlane.attack(s);//升级攻击
	//     }, 2000)
	// }, 2000)
	/*控制飞机移动*/
	s.stage.addEventListener(annie.MouseEvent.MOUSE_MOVE, function (e) {
		// var point = s.globalToLocal(new annie.Point(e.stageX, e.stageY));
		// trace(point);
		if (battlePlane.status == 0) {
			battlePlane.x = e.stageX;
			battlePlane.y = e.stageY - 160;

			//可拖动区域
			if (battlePlane.x < s.stage.viewRect.x + battlePlane.planeW / 2) {
				battlePlane.x = s.stage.viewRect.x + battlePlane.planeW / 2;
			} else if (battlePlane.y < 0) {
				battlePlane.y = 0;
			} else if (battlePlane.x > s.stage.viewRect.x + s.stage.viewRect.width - battlePlane.planeW / 2) {
				battlePlane.x = s.stage.viewRect.x + s.stage.viewRect.width - battlePlane.planeW / 2;
			} else if (battlePlane.y > s.stage.viewRect.height - battlePlane.planeH) {
				battlePlane.y = s.stage.viewRect.height - battlePlane.planeH;
			}

		}
	});

	s.creatEnemPlanes(400);//2秒生产一架敌机
	// trace(s.children);
	if (s.battlePlane && s.battlePlane.status == 0 && s.battlePlane.attackLevel < 3) {
		s.creatTonicTimeId = setInterval(function () {
			s.addTonic();
		}, 10000)
	} else {
		clearInterval(s.creatTonicTimeId);
		s.creatTonicTimeId = null;
	}

}

/**
 * 添加到舞台
 * @param e
 */
feiji.GamingPage.prototype.onAddToStage = function (e) {
	var s = this;
	if (!s.countScoreManager) {
		s.countScoreManager = new feiji.ScoreCon();//分数管理类
	}
	s.gameAct();//激活游戏
	/*添加红色大敌机*/
	annie.globalDispatcher.addEventListener('initEnemyRed', function (e) {
		if (!s.boosRed) {
			s.boosRed = new feiji.EnemyRed();
			if (!s.boosRed.hitTestObj) {
				s.boosRed.hitTestObj = s.battlePlane;
			}
			s.battlePlane.enemys.push(s.boosRed);//为飞机添加敌对目标
			s.addChildAt(s.boosRed, 2);
		}
	})
	/*添加boss敌机*/
	annie.globalDispatcher.addEventListener('initEnemyBoss', function (e) {
		if (!s.finalBoss) {
			s.finalBoss = new feiji.EnemyBoss();
			if (!s.finalBoss.hitTestObj) {
				s.finalBoss.hitTestObj = s.battlePlane;
			}
			s.battlePlane.enemys.push(s.finalBoss);//为飞机添加敌对目标
			s.addChildAt(s.finalBoss, 2);
		}
	})
	/*战机提升攻击力*/
	annie.globalDispatcher.addEventListener('imporveAttack', function (e) {
		if (s.battlePlane.attackLevel < 3) {
			s.battlePlane.attackLevel++;
		}
		s.battlePlane.attack(s);
	})
}

feiji.GamingPage.prototype.creatEnemPlanes = function (rate) {
	var s = this,
		enemySystem = new feiji.EnemyManager();
	if (!s.creatEnemyPlanesTimeId) {
		s.creatEnemyPlanesTimeId = setInterval(function () {
			if (s.battlePlane.status == 0) {
				/*生成敌机*/
				var smallEnemyPlane = enemySystem.getEnemy();
				if (!smallEnemyPlane.hitTestObj) {
					smallEnemyPlane.hitTestObj = s.battlePlane;
				}
				s.battlePlane.enemys.push(smallEnemyPlane);//为飞机添加敌对目标
				s.addChildAt(smallEnemyPlane, 1);
			} else {
				//判断战机生命

			}
		}, rate);
	}
}
/*掉落弹药*/
feiji.GamingPage.prototype.addTonic = function () {
	var s = this,
		tonic = new feiji.Tonic();
	if (!tonic.hitTestObj) {
		tonic.hitTestObj = s.battlePlane;
	}
	s.addChildAt(tonic, 3);
}
