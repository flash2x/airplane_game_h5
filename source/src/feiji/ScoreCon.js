AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.ScoreCon=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*/s.score_txt=null;/*_a2x_need_end*/
	annie.initRes(s,"feiji","ScoreCon");
	//your code here

	s.addEventListener(annie.Event.ADD_TO_STAGE, function (e) {

	})
	//记录分数
	annie.globalDispatcher.addEventListener('scoreCountEvent', function (e) {
		s.countNum += 10;
		s.score_txt.text = s.countNum;
		// trace('当前分数：' + s.countNum);
		if (s.countNum == 200) {
			annie.globalDispatcher.dispatchEvent(new annie.Event('initEnemyRed'));//红色飞机出现
		} else if (s.countNum == 600) {
			annie.globalDispatcher.dispatchEvent(new annie.Event('initEnemyBoss'));//大boss飞机出现
		}
	})
};
A2xExtend(feiji.ScoreCon,annie.Sprite);
feiji.ScoreCon.prototype.countNum = 0;
/*计算游戏中分数*/
feiji.ScoreCon.prototype.countScore = function () {
	var s = this;
	s.countNum += 10;
	s.score_txt.text = s.countNum;
	if (s.countNum == 800) {
		trace('中型飞机');
	}
}
/*重设分数*/
feiji.ScoreCon.prototype.resetScore = function () {
	var s = this;
	s.countNum = 0;
	s.score_txt.text = s.countNum;
}
/*累计通过奖励分数*/
feiji.ScoreCon.prototype.addLevelScore = function (score) {
	var s = this;
	s.countNum += score;
	s.score_txt.text = s.countNum;
}
