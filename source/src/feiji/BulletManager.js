AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.BulletManager=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","BulletManager");
	//your code here
	annie.globalDispatcher.addEventListener('recoveryBulletEvent', function (e) {
		s.recoveryBullet(e.data);//回收子弹到子弹池中
		// trace(s.bulletPool.length);
	})
};
A2xExtend(feiji.BulletManager,annie.Sprite);
feiji.BulletManager.prototype.bulletPool = [];//子弹池
feiji.BulletManager.prototype.attackTargets = [];//子弹的攻击目标数组
/*获取子弹方法*/
feiji.BulletManager.prototype.getBullet = function () {
	var s = this,
		bullet;
	if (s.bulletPool.length > 0) {
		bullet = s.bulletPool.shift();
	} else {
		bullet = new feiji.Bullet();
	}
	bullet.targetEnemys = s.attackTargets;//告诉子弹的攻击目标
	return bullet;//返回子弹类实例
}

/**
 * 子弹回收
 * @param bullet
 */
feiji.BulletManager.prototype.recoveryBullet = function (bullet) {
	var s = this;
	if (!bullet) {
		throw new Error('bullet参数不能为空');
	}
	bullet.gotoAndStop(1);//回复子弹默认类型
	bullet.recoveryBulletEvent = null;
	bullet.status = 0;
	s.bulletPool.push(bullet);
}
/*销毁*/
feiji.BulletManager.prototype.destroy = function () {

}
