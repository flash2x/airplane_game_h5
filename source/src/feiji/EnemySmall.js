AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.EnemySmall=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","EnemySmall");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.EnemySmall,annie.MovieClip);
feiji.EnemySmall.prototype.status = 0;//0正常状态，1子弹爆炸
feiji.EnemySmall.prototype.EsHeight = 120;
feiji.EnemySmall.prototype.EsWidth = 98;
feiji.EnemySmall.prototype.speed = 4;
feiji.EnemySmall.prototype.blood = 10;//敌机血量
feiji.EnemySmall.prototype.enemyType = 'enemySmall';
feiji.EnemySmall.prototype.delDiePlaneEvent = null;
feiji.EnemySmall.prototype.recoveryEnemyEvent = null;
feiji.EnemySmall.prototype.scoreCountEvent = null;
feiji.EnemySmall.prototype.hitTestObj = null;
feiji.EnemySmall.prototype.bomb = function () {
	var s = this;
	s.removeEventListener(annie.Event.ENTER_FRAME, s.emh);
	s.emh = null;
	s.gotoAndStop(2);
	s.status = 1;//爆炸状态
	setTimeout(function () {
		if (!s.delDiePlaneEvent) {
			s.delDiePlaneEvent = new annie.Event('delDiePlane');
		}
		annie.globalDispatcher.dispatchEvent(s.delDiePlaneEvent, s);//抛出移除爆炸敌机事件，Fighter类在接收

		if (!s.recoveryEnemyEvent) {
			s.recoveryEnemyEvent = new annie.Event('recoveryEnemyEvent');
		}
		annie.globalDispatcher.dispatchEvent(s.recoveryEnemyEvent, s);//抛出回收敌机事件，回收视窗以外的敌机到敌机池中

		//抛发分数事件
		if (!s.scoreCountEvent) {
			s.scoreCountEvent = new annie.Event('scoreCountEvent');
		}
		annie.globalDispatcher.dispatchEvent(s.scoreCountEvent);//抛出记录分数事件
		if (s.stage) {
			s.parent.removeChild(s);//把视窗外的敌机移除舞台
		}
	}, 400);
}
/**
 * 添加到舞台
 * @param e
 */
feiji.EnemySmall.prototype.onAddToStage = function (e) {
	var s = this;
	s.addEventListener(annie.Event.ENTER_FRAME, s.emh = s.EnemyMovingHandler.bind(s));
	//初始化敌机的有效出现位置
	var validX,
		randomX = Math.floor(Math.random() * s.stage.desWidth);
	if (randomX < s.stage.viewRect.x + s.EsWidth / 2) {
		validX = s.stage.viewRect.x + s.EsWidth / 2 + 20;
	} else if (randomX > (s.stage.viewRect.x + s.stage.viewRect.width - s.EsWidth / 2)) {
		validX = s.stage.viewRect.x + s.stage.viewRect.width - s.EsWidth / 2 - 20;
	} else {
		validX = randomX;
	}
	s.x = validX;
	s.y = 0;
}
/**
 *
 * @param e
 * @constructor
 */
feiji.EnemySmall.prototype.EnemyMovingHandler = function (e) {
	var s = this;
	s.enemyMoving();//敌机移动
}
feiji.EnemySmall.prototype.enemyMoving = function () {
	var s = this;
	if (s.stage) {
		if (s.y > s.stage.desHeight + s.EsHeight) {
			s.removeEventListener(annie.Event.ENTER_FRAME, s.emh);
			s.emh = null;
			s.gotoAndStop(1);
			if (!s.recoveryEnemyEvent) {
				s.recoveryEnemyEvent = new annie.Event('recoveryEnemyEvent');
			}
			annie.globalDispatcher.dispatchEvent(s.recoveryEnemyEvent, s);//抛出回收敌机事件，回收视窗以外的敌机到敌机池中
			s.parent.removeChild(s);//把视窗外的敌机移除舞台
		}
	}
	//敌机碰到战机
	if (s.hitTestObj && s.hitTestObj.status == 0 && s.status == 0 && getDistance(s.hitTestObj.x, s.hitTestObj.y, s.x, s.y) < 40) {
		s.removeEventListener(annie.Event.ENTER_FRAME, s.emh);
		s.emh = null;
		if (s.hitTestObj) {
			s.hitTestObj.bomb();//战机爆炸
		}
		s.bomb();//敌机爆炸
	}
	s.y += s.speed;
}
