AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.LiveCon=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*/s.live_txt=null;/*_a2x_need_end*/
	annie.initRes(s,"feiji","LiveCon");
	//记录分数
	annie.globalDispatcher.addEventListener('liveCountEvent', function (e) {
		if (s.liveCount > 0) {
			s.liveCount -= 1;
		}
		s.live_txt.text = s.liveCount;
		// trace('生命数：' + s.liveCount);
	})
};
A2xExtend(feiji.LiveCon,annie.Sprite);
feiji.LiveCon.prototype.liveCount = 1;
