AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.EnemyManager=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","EnemyManager");
	//your code here
	annie.globalDispatcher.addEventListener('recoveryEnemyEvent', function (e) {
		s.recoveryEnemy(e.data);//回收敌机到敌机池中
		// trace(s.enemyPool.length);
	})
};
A2xExtend(feiji.EnemyManager,annie.Sprite);
feiji.EnemyManager.prototype.enemyPool = [];//敌机池

/*获取敌机方法*/
feiji.EnemyManager.prototype.getEnemy = function () {
	var s = this,
		enemy;
	if (s.enemyPool.length > 0) {
		enemy = s.enemyPool.shift();
	} else {
		enemy = new feiji.EnemySmall();//生成小敌机
	}
	return enemy;//返回小敌机类实例
}

/**
 * 敌机回收
 * @param enemy
 */
feiji.EnemyManager.prototype.recoveryEnemy = function (enemy) {
	var s = this;
	if (!enemy) {
		throw new Error('enemy参数不能为空');
	}
	enemy.hitTestObj = null;
	enemy.gotoAndStop(1);
	enemy.status = 0;
	if (enemy.enemyType == "enemySmall") {
		enemy.blood = 10;
	}

	s.enemyPool.push(enemy);
}
