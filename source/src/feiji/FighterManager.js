AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.FighterManager=function(){
	var s = this;
	annie.Sprite.call(s);
	/*_a2x_need_start*//*_a2x_need_end*/
	annie.initRes(s,"feiji","FighterManager");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.FighterManager,annie.Sprite);
feiji.FighterManager.prototype.initUI = function () {
	var s = this;
	//f2x_auto_created_init_start

	//f2x_auto_created_init_end

};
feiji.FighterManager.prototype.attackSystem = null;//弹道系统
/**
 * 攻击方法
 */
feiji.FighterManager.prototype.attack = function () {

}
/**
 * 添加到舞台
 * @param e
 */
feiji.FighterManager.prototype.onAddToStage = function (e) {
	var s = this;
	if (!s.attackSystem) {
		s.attackSystem=new feiji.BulletManager();//初始化弹道系统
	}
};
