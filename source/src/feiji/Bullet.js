AnnieRoot.feiji=AnnieRoot.feiji||{};
feiji.Bullet=function(){
	var s = this;
	annie.MovieClip.call(s);
	/*_a2x_need_start*/s.boom_mc=null;/*_a2x_need_end*/
	annie.initRes(s,"feiji","Bullet");
	//your code here
	s.addEventListener(annie.Event.ADD_TO_STAGE, s.onAddToStage.bind(s));
};
A2xExtend(feiji.Bullet,annie.MovieClip);
feiji.Bullet.prototype.status = 0;//0正常状态，1子弹爆炸
feiji.Bullet.prototype.recoveryBulletEvent = null;
feiji.Bullet.prototype.skin = 1;//子弹的皮肤
feiji.Bullet.prototype.type = 1;//子弹类型，1红色子弹、2蓝色子弹
feiji.Bullet.prototype.speed = 24;//子弹速度
feiji.Bullet.prototype.attackRange = 48;//子弹爆炸范围
feiji.Bullet.prototype.targetEnemys = [];//敌机数组

/*子弹状态变化*/
feiji.Bullet.prototype.changeStatus = function (status) {
	var s = this;
	s.status = status;
}
/**
 * 添加到舞台
 * @param e
 */
feiji.Bullet.prototype.onAddToStage = function (e) {
	var s = this;
	if (!s.recoveryBulletEvent) {
		s.recoveryBulletEvent = new annie.Event('recoveryBulletEvent');
	}
	s.addEventListener(annie.Event.ENTER_FRAME, s.bmh = s.bulletMovingHandler.bind(s));
	// trace(s);
	// s.y=100;

	//爆炸动画完成
	s.boom_mc.addEventListener(annie.Event.END_FRAME, s.bef = function (e) {
		if (s.status == 1) {
			s.boom_mc.removeEventListener(annie.Event.ENTER_FRAME, s.bef);
			s.bef = null;
			annie.globalDispatcher.dispatchEvent(s.recoveryBulletEvent, s);//抛出回收子弹事件，回收视窗以外的子弹到子弹池中
			if (s.stage) {
				s.parent.removeChild(s);//把击中目标的子弹移除舞台
				// s.boom_mc.removeEventListener(annie.Event.END_FRAME, s.bef);
			}
		}
	})
}
/**
 * 子弹移动
 * @param e
 */
feiji.Bullet.prototype.bulletMovingHandler = function (e) {
	var s = this;
	s.bulletMoving();
}

/**
 *
 */
feiji.Bullet.prototype.bulletMoving = function () {
	var s = this;
	/*移除屏幕--情况1*/
	if (s.y < -86) {
		s.removeEventListener(annie.Event.ENTER_FRAME, s.bmh);
		s.bmh = null;
		annie.globalDispatcher.dispatchEvent(s.recoveryBulletEvent, s);//抛出回收子弹事件，回收视窗以外的子弹到子弹池中
		s.parent.removeChild(s);//把视窗外的子弹移除舞台
	}

	/*击中目标--情况2*/
	if (!s.parent) {
		return;
	}
	var targetEnemyLeng = s.targetEnemys.length,
		// bulletPoint = s.parent.localToGlobal(new annie.Point(s.x, s.y)),//子弹原点坐标
		dis;
	// trace('敌机数量：' + targetEnemyLeng);
	if (targetEnemyLeng > 0) {
		for (var i = 0; i < targetEnemyLeng; i++) {
			var targetEnemy = s.targetEnemys[i];
			if (targetEnemy.enemyType == "redBoss" || targetEnemy.enemyType == "rockBoss") {
				s.attackRange = 80;
			} else {
				s.attackRange = 48;
			}
			// if (targetEnemy.status == 0 && targetEnemy.hitTestPoint(bulletPoint)) {
			if (targetEnemy.status == 0 && getDistance(targetEnemy.x, targetEnemy.y, s.x, s.y) < s.attackRange) {
				s.removeEventListener(annie.Event.ENTER_FRAME, s.bmh);
				s.bmh = null;
				s.gotoAndStop(3);//播放子弹击中爆炸效果
				s.status == 1;
				switch (targetEnemy.enemyType) {
					case "enemySmall":
						//敌机爆炸事件
						targetEnemy.bomb();
						break;
					case 'redBoss':
						if (targetEnemy.blood <= 0) {
							targetEnemy.bomb();
						} else {
							targetEnemy.blood -= 10;
						}
						break;
					case'rockBoss':
						if (targetEnemy.blood <= 0) {
							targetEnemy.bomb();
						} else {
							targetEnemy.blood -= 10;
						}
						break;
					default:
						break;
				}


				// setTimeout(function () {
				//     annie.globalDispatcher.dispatchEvent('recoveryBulletEvent', s);//抛出回收子弹事件，回收视窗以外的子弹到子弹池中
				//
				//     s.parent.removeChild(s);//把击中目标的子弹移除舞台
				//     // s.targetEnemys.splice(i, 1);//删除歼灭的攻击目标
				//
				// }, 400);
			}
		}
	}
	s.y -= s.speed;
}
